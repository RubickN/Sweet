#coding=utf-8
import os
import xlrd
import sys, traceback
import chardet
import csv
import json
CONFIGDEFAULTSTR = "DEFAULT"


default_encoding = "utf-8"
if default_encoding != sys.getdefaultencoding():
    reload(sys)
    sys.setdefaultencoding(default_encoding)


def get_config():
    configuration_path = os.path.join(os.path.dirname(__file__), "configuration")
    with open(configuration_path, "r") as f:
        config = f.read()
    config = json.loads(config)
    return config


def load_default_configuration():
    configs = get_config()
    if configs["Resource_Path"] == CONFIGDEFAULTSTR:
        configs["Resource_Path"] = os.path.join(os.path.dirname(__file__), "Resource")
    if configs["Result_Path"] == CONFIGDEFAULTSTR:
        configs["Result_Path"] = os.path.join(os.path.dirname(__file__), "Result")

    if os.path.exists(configs["Resource_Path"]) is False:
        os.mkdir(configs["Resource_Path"])

    if os.path.exists(configs["Result_Path"]) is False:
        os.mkdir(configs["Result_Path"])

    return configs


def get_config():
    configuration_path = os.path.join(os.path.dirname(__file__), "configuration")
    with open(configuration_path, "r") as f:
        config = f.read()
    config = json.loads(config)
    return config


def get_content_from_excel(filename, config, title=[]):
    multi_field_index = config["multi_field_index"]
    rank_index = config["Rank_field_index"]
    split_char = config["Split_Char"]
    data = xlrd.open_workbook(filename)
    sheets = data.sheets()
    all_data = []
    result = []
    for sheet in sheets:
        nrows = sheet.nrows
        start_row = 0
        if nrows == 0:
            continue
        if len(title) == 0:
            title_str = sheet.row_values(0)
            first_row = sheet.row_values(1)
            multi_field_data = str(first_row[multi_field_index]).split(split_char)
            multi_field_length = len(multi_field_data)
            title = handle_title(title_str, rank_index, multi_field_length)
            start_row = 1
            result.append(title)
        for i in range(start_row, nrows):
            row_content = sheet.row_values(i)
            result.extend(split_table(row_content, split_char, rank_index, multi_field_index))

    return result


def save_file(content, filename, title=[]):
    csvfile = file(filename, 'wb')
    writer = csv.writer(csvfile)
    if len(title) > 0:
        title = map(decode, title)
        writer.writerow(title)
    for each in content:
        row = map(decode, each)
        writer.writerow(row)
    csvfile.close()


def split_table(row_content, split_char, rank_index, multi_field_index):
    result = []
    multi_field_data = str(row_content[multi_field_index]).split(split_char)
    multi_field_length = len(multi_field_data)
    rank_data = row_content[rank_index:rank_index + multi_field_length]
    for i, data in enumerate(multi_field_data):
        tmp = row_content[0:multi_field_index]
        tmp.append(data)
        if rank_index - multi_field_index > 1:
            tmp.extend(row_content[multi_field_index + 1: rank_index])
        tmp.append(rank_data[i])
        tmp.extend(row_content[rank_index + multi_field_length:])
        # tmp = copy.copy(row_content)
        # tmp[multi_field_index] = data0
        # tmp[rank_index] = rank_data[i]
        result.append(tmp)
    return result


def handle_title(title, rank_index, rank_length):
    result = title[0:rank_index]
    result.extend(title[rank_index+rank_length - 1:])
    return result


def decode(content):
    try:
        if isinstance(content, (str, unicode)):
            content = content.decode("utf-8").encode("gbk")
        return content
    except Exception, e:
        print transfer_decode("字符%s出现异常，已用#代替" % content)
    return "#"


def transfer_decode(strs):
    return strs.decode(chardet.detect(strs)['encoding'])


def main():
    print transfer_decode("载入默认配置.......")
    config = load_default_configuration()
    print transfer_decode("准备开始读入数据文件.....")
    for each_file in os.listdir(config['Resource_Path']):
        if "xlsx" not in each_file or "xls" not in each_file:
            continue
        print transfer_decode("读入文件")
        file_path = os.path.join(config['Resource_Path'], each_file)
        result_path = os.path.join(config['Result_Path'], str(each_file).split(".")[0] + ".csv")
        content = get_content_from_excel(file_path, config)
        print transfer_decode("保存文件")
        save_file(content, result_path, title=[])
    print transfer_decode("完事收工")


if __name__ == "__main__":
    try:
        print transfer_decode("*****************************程序开始*********************************")
        main()
        print transfer_decode("*****************************程序结束*********************************")
        print ""
        print transfer_decode("按 Enter 键结束程序")
    except Exception, e:
        print transfer_decode("*****************************错误结束*********************************")
        print str(e)
        print traceback.print_stack()
        print ""
        print ""
        print ""
        print transfer_decode("出BUG啦，请速速联系小哥")

    name = raw_input()